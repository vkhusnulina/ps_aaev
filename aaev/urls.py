from django.conf.urls import patterns, include, url
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.contrib import admin

urlpatterns = patterns('',
                       url(r'^admin/', include(admin.site.urls)),
                       )

# ------- Home
urlpatterns += patterns('web.views.setupView',
                        (r'^setup_aaev$', 'initial_setup'),
                        (r'^load_test_data', 'load_test_data'),
                        (r'^remove_test_data', 'remove_test_data')
                        )

urlpatterns += patterns('web.views.inicioView',
                        (r'^$', 'home'),
                        (r'^ayuda/$', 'ayuda'),
                        (r'^perfil/$', 'perfil'),
                        (r'^nuevo_administrador$', 'new_admin'),
                        )

# ------- Usuarios
urlpatterns += patterns('web.views.usersView',
                        (r'^usuario/editar/$', 'editar_usuario'),
                        # ----- Alumnos
                        (r'^alumnos/$', 'alumnos'),
                        (r'^alumnos/agregar/csv/$', 'leer_csv'),
                        (r'^alumnos/procesar/$', 'procesar_alumnos'),
                        (r'^alumnos/agregar/$', 'agregar_alumnos'),
                        (r'^alumno/(\d{1,5})/$', 'ver_alumno'),
                        # ----- Profesores
                        (r'^profesores/$', 'profesores'),
                        (r'^profesores/agregar/$', 'agregar_profesor'),
                        (r'^profesor/(\d{1,5})/$', 'ver_profesor'),
                        # ----- Administracion
                        (r'^nuevo_administrador/$', 'new_admin_user')
                        )

# ------- Materias
urlpatterns += patterns('web.views.materiasView',
                        (r'^nueva_materia/$', 'nueva_materia'),
                        (r'^materias/$', 'listar'),
                        (r'^inhabilitar_materia/$', 'inhabilitar'),
                        (r'^habilitar_materia/$', 'habilitar'),
                        (r'^editar_materia/$', 'editar'),
                        (r'^docentes_materia/$', 'docentes'),
                        (r'^docente/$', 'docente'),
                        (r'^materia/(\d{1,100})/$', 'perfil'),
                        )

# ------- Temas
urlpatterns += patterns('web.views.temasView',
                        (r'^temas/$', 'listar_con_preguntas'),
                        (r'^asignar_tema/$', 'get_index'),
                        (r'^asignar_tema/temas_de_materia/(\d{1,5})/$', 'temas_de_materia'),
                        (r'^asignar_tema/temas_de_materia_combo/(\d{1,5})/$', 'temas_de_materia_combo'),
                        (r'^agregar_tema/$', 'crear'),
                        )

# ------- Preguntas
urlpatterns += patterns('web.views.preguntasView',
                        (r'^pregunta/ver/(\d{1,5})/$', 'ver'),
                        (r'^pregunta/editar/$', 'editar'),
                        (r'^inhabilitar_pregunta/$', 'inhabilitar'),
                        (r'^habilitar_pregunta/$', 'habilitar'),
                        (r'^preguntas/$', 'listar'),

                        (r'^preguntas/preguntas_del_tema/(\d{1,5})/$', 'preguntas_incompatibles'),
                        (r'^respuestas/form/(\d{1,5})/$', 'respuestas_form'),
                        (r'^nueva_pregunta/$', 'nueva_pregunta'),
                        (r'^preguntas/index/$', 'lista_pregunta')
                        )

# ------- Evaluaciones
urlpatterns += patterns('web.views.evaluacionesView',
                        (r'^evaluaciones/$', 'get_index'),
                        (r'^evaluacion/ver/(\d{1,5})/$', 'mostrar_evaluacion'),
                        (r'^evaluacion/comenzar/(\d{1,5})/$', 'mostrar_evaluacion_resolver'),
                        (r'^evaluacion/resolver/(\d{1,5})$', 'evaluacion_resolver'),
                        (r'^evaluar/(\d{1,5})/(\d{1,5})$', 'evaluar'),
                        (r'^nueva_evaluacion/$', 'nueva_evaluacion_index'),
                        (r'^asignar_evaluacion/(\d{1,5})/$', 'asignar_evaluacion'),
                        (r'^nueva_evaluacion/crear/$', 'nueva_evaluacion'),
                        (r'^nueva_evaluacion/configuraciones/$', 'nueva_evaluacion_configuraciones'),
                        (r'^evaluaciones/temas_materia_checkbox/(\d{1,5})/$', 'temas_materia_checkbox'),
                        )

# ------- Login & Logout
urlpatterns += patterns('',
                        url(r'^login/$', 'django.contrib.auth.views.login', {'template_name': 'login.html'}),
                        )
urlpatterns += patterns('web.views.auth.view_auth',
                        url(r'^logout/$', 'logout'),
                        )

# ------- Archivos estaticos (css, js e imagenes)
urlpatterns += staticfiles_urlpatterns()
