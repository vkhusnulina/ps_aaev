"""
Django settings for aaev project.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.7/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
import socket

BASE_DIR = os.path.dirname(os.path.dirname(__file__))

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.7/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '99!ch6o)+(0=l9o1vfmwh(_=tk_xc@o9nt2k*k0f78(ev6ypei'

from os import environ

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = environ.get('DJANGO_DEBUG', True)
TEMPLATE_DEBUG = DEBUG

ALLOWED_HOSTS = ['localhost:8000', 'vkhusnulina.pythonanywhere.com', 'laboratorio3.sistemas.unla.edu.ar']

# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'web'
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'aaev.urls'
WSGI_APPLICATION = 'aaev.wsgi.application'

# Database
# https://docs.djangoproject.com/en/1.7/ref/settings/#databases
# Update database configuration with $DATABASE_URL.
# import dj_database_url

# db_from_env = dj_database_url.config(conn_max_age=500)

DATABASES = {
    'pythonanywhere': {
        'NAME': 'vkhusnulina$aaev',
        'ENGINE': 'django.db.backends.mysql',
        'USER': 'vkhusnulina',
        'PASSWORD': '5lezie5l',
        'HOST': 'vkhusnulina.mysql.pythonanywhere-services.com',
        'PORT': '',  # Set to empty string for default.
    },
    'localhost': {
        'NAME': 'aaev',
        'ENGINE': 'django.db.backends.mysql',
        'USER': 'root',
        'PASSWORD': 'root',
        'HOST': 'localhost',  # Empty for localhost through domain sockets or '127.0.0.1' for localhost through TCP.
        'PORT': '3306',  # Set to empty string for default.
    },
    'unla': {
        'NAME': 'ps_aae2014',
        'ENGINE': 'django.db.backends.mysql',
        'USER': 'aae2014',
        'PASSWORD': '98f83f1',
        'HOST': 'localhost',  # Empty for localhost through domain sockets or '127.0.0.1' for localhost through TCP.
        'PORT': '3306',  # Set to empty string for default.
    },
}

default_database = os.environ.get('DJANGO_DATABASE', 'pythonanywhere')
DATABASES['default'] = DATABASES[default_database]

# Internationalization
# https://docs.djangoproject.com/en/1.7/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True

SESSION_SERIALIZER = 'django.contrib.sessions.serializers.JSONSerializer'

# Absolute path to the directory statics files should be collected to.
# Don't put anything in this directory yourself; store your statics files
# in apps' "statics/" subdirectories and in STATICFILES_DIRS.
# Example: "/var/www/example.com/statics/"
STATIC_ROOT = os.path.join(BASE_DIR, "static")

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.7/howto/static-files/

STATIC_URL = '/static/'
# Additional locations of statics files
STATICFILES_DIRS = (
    (os.path.join(BASE_DIR, "aaev/assets"),)
)

# List of finder classes that know how to find statics files in
# various locations.
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    #    'django.contrib.staticfiles.finders.DefaultStorageFinder',
)

TEMPLATE_DIRS = (
    os.path.join(os.path.dirname(__file__), 'templates').replace('\\', '/'),
    # Put strings here, like "/home/html/django_templates" or "C:/www/django/templates".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
)

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
    #     'django.template.loaders.eggs.Loader',
)

TEMPLATE_CONTEXT_PROCESSORS = (
    "django.core.context_processors.request",
    "django.contrib.auth.context_processors.auth"
)

SESSION_EXPIRE_AT_BROWSER_CLOSE = True

# Auto logout delay in seconds
SESSION_COOKIE_AGE = 60 * 30

# Authentication
LOGIN_REDIRECT_URL = '/'
LOGIN_URL = '/login'
LOGOUT_URL = '/logout'
