"""
WSGI config for aaev project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/howto/deployment/wsgi/
"""

import os

import sys

path = '/var/www/html/ps/2014/aae/ps_aaev/aaev'
if path not in sys.path:
    sys.path.append(path)
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "aaev.settings")

from django.core.wsgi import get_wsgi_application

application = get_wsgi_application()
