from django.contrib.auth.models import User
from django.db import models


# Create your models here.


class Perfil(models.Model):
    usuario = models.OneToOneField(User)
    dni = models.CharField(max_length=50, unique=True)
    telefono = models.CharField(max_length=50)

    def __unicode__(self):
        return self.dni

    class Meta:
        db_table = 'perfiles'


class TipoPregunta(models.Model):
    tipo = models.CharField(max_length=100)
    enunciado = models.CharField(max_length=200)

    @staticmethod
    def desarrollar():
        return 1

    @staticmethod
    def seleccion_multiple():
        return 2

    @staticmethod
    def unir_con_flecha():
        return 3

    @staticmethod
    def verdadero_o_falso():
        return 4

    def __unicode__(self):
        return self.tipo

    class Meta:
        db_table = 'tipos_pregunta'


class Materia(models.Model):
    codigo = models.IntegerField()
    creador = models.ForeignKey(User, related_name='usuario_creador')
    nombre = models.CharField(max_length=100)
    descripcion = models.TextField()
    eliminada = models.BooleanField(default=False)
    docentes = models.ManyToManyField(User, related_name='docente', db_table='materia_docente')
    alumnos = models.ManyToManyField(User, related_name='alumno', db_table='materia_alumno')

    @staticmethod
    def get_list(user):
        if user.is_superuser:
            return Materia.objects.all()
        elif user.is_staff:
            return Materia.objects.filter(docentes__id=user.id)
        else:
            return Materia.objects.filter(alumnos__id=user.id)

    def __unicode__(self):
        return self.nombre

    class Meta:
        db_table = 'materias'


class Tema(models.Model):
    materia = models.ForeignKey(Materia)
    nombre = models.CharField(max_length=100)
    descripcion = models.TextField()

    def __unicode__(self):
        return self.nombre

    class Meta:
        db_table = 'temas'


class Pregunta(models.Model):
    tipo = models.ForeignKey(TipoPregunta)
    tema = models.ForeignKey(Tema)
    descripcion = models.TextField()
    urlImagen = models.URLField()
    ultimoUso = models.IntegerField(default=0)
    eliminada = models.BooleanField(default=False)
    # from_pregunta_id, to_pregunta_id
    preguntasIncompatibles = models.ManyToManyField("self", related_name='to_pregunta',
                                                    db_table='preguntas_incompatibles')

    def __unicode__(self):
        return self.descripcion

    class Meta:
        db_table = 'preguntas'


class Evaluacion(models.Model):
    nombre = models.CharField(max_length=50)
    fecha_creacion = models.DateField()
    fecha_hora_comienzo = models.DateTimeField()
    duracion_minutos = models.IntegerField()
    acepta_entrega_tardia = models.BooleanField(default=False)
    materia = models.ForeignKey(Materia)
    preguntas = models.ManyToManyField(Pregunta, db_table='evaluacion_pregunta')

    @staticmethod
    def get_list(user):
        if user.is_superuser:
            return Evaluacion.objects.all()
        elif user.is_staff:
            materias = Materia.get_list(user)
            materias_pk = materias.values_list('id', flat=True)
            return Evaluacion.objects.filter(materia__in=materias_pk)
        else:
            evaluaciones_del_alumno = EvaluacionesUsuarios.objects.filter(usuario=user).values_list('evaluacion__pk',
                                                                                                    flat=True)
            return Evaluacion.objects.filter(id__in=evaluaciones_del_alumno)

    def __unicode__(self):
        return self.nombre

    class Meta:
        db_table = 'evaluaciones'


class EvaluacionesUsuarios(models.Model):
    evaluacion = models.ForeignKey(Evaluacion)
    usuario = models.ForeignKey(User)
    clave_acceso = models.CharField(max_length=50, null=True)
    fecha_hora_comienzo = models.DateTimeField(null=True)
    fecha_hora_final = models.DateTimeField(null=True)
    nota_final = models.FloatField(null=True)

    def __unicode__(self):
        return self.clave_acceso

    class Meta:
        db_table = 'evaluaciones_usuarios'


class Respuesta(models.Model):
    pregunta = models.ForeignKey(Pregunta)
    descripcion = models.TextField()
    url_imagen = models.URLField()
    correcta = models.BooleanField(default=False)

    def __unicode__(self):
        return self.descripcion

    class Meta:
        db_table = 'respuestas'


class EvaluacionPreguntaRespuestaUsuario(models.Model):
    alumno = models.ForeignKey(User)
    evaluacion = models.ForeignKey(Evaluacion)
    pregunta = models.ForeignKey(Pregunta)
    respuesta = models.ForeignKey(Respuesta, null=True)
    respuesta_texto = models.TextField()

    def __unicode__(self):
        return self.respuesta_texto

    class Meta:
        db_table = 'evaluacion_pregunta_respuesta_usuario'
