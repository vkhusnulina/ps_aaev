# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('web', '0006_evaluacionpreguntarespuestausuario_alumno'),
    ]

    operations = [
        migrations.AddField(
            model_name='evaluacionpreguntarespuestausuario',
            name='evaluacion',
            field=models.ForeignKey(default=1, to='web.Evaluacion'),
            preserve_default=False,
        ),
    ]
