# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('web', '0004_auto_20160719_0917'),
    ]

    operations = [
        migrations.AlterField(
            model_name='evaluacionesusuarios',
            name='clave_acceso',
            field=models.CharField(max_length=50, null=True),
        ),
        migrations.AlterField(
            model_name='evaluacionesusuarios',
            name='fecha_hora_comienzo',
            field=models.DateTimeField(null=True),
        ),
        migrations.AlterField(
            model_name='evaluacionesusuarios',
            name='fecha_hora_final',
            field=models.DateTimeField(null=True),
        ),
        migrations.AlterField(
            model_name='evaluacionesusuarios',
            name='nota_final',
            field=models.FloatField(null=True),
        ),
        migrations.AlterField(
            model_name='evaluacionpreguntarespuestausuario',
            name='respuesta',
            field=models.ForeignKey(to='web.Respuesta', null=True),
        ),
    ]
