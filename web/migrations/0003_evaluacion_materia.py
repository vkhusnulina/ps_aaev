# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('web', '0002_auto_20160618_2247'),
    ]

    operations = [
        migrations.AddField(
            model_name='evaluacion',
            name='materia',
            field=models.ForeignKey(default=1, to='web.Materia'),
            preserve_default=False,
        ),
    ]
