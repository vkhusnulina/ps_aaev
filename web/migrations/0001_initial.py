# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Evaluacion',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nombre', models.CharField(max_length=50)),
                ('fecha_creacion', models.DateField()),
                ('fecha_hora_comienzo', models.DateTimeField()),
                ('duracion_minutos', models.IntegerField()),
                ('acepta_entrega_tardia', models.BooleanField()),
            ],
            options={
                'db_table': 'evaluaciones',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='EvaluacionesUsuarios',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('clave_acceso', models.CharField(max_length=50)),
                ('fecha_hora_comienzo', models.DateTimeField()),
                ('fecha_hora_final', models.DateTimeField()),
                ('nota_final', models.FloatField()),
                ('evaluacion', models.ForeignKey(to='web.Evaluacion')),
                ('usuario', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'db_table': 'evaluaciones_usuarios',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='EvaluacionPreguntaRespuestaUsuario',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('respuesta_texto', models.TextField()),
            ],
            options={
                'db_table': 'evaluacion_pregunta_respuesta_usuario',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Materia',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('codigo', models.IntegerField()),
                ('nombre', models.CharField(max_length=100)),
                ('descripcion', models.TextField()),
                ('alumnos', models.ManyToManyField(related_name=b'alumno', db_table=b'materia_alumno', to=settings.AUTH_USER_MODEL)),
                ('creador', models.ForeignKey(related_name=b'usuario_creador', to=settings.AUTH_USER_MODEL)),
                ('docentes', models.ManyToManyField(related_name=b'docente', db_table=b'materia_docente', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'db_table': 'materias',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Perfil',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('dni', models.CharField(unique=True, max_length=50)),
                ('telefono', models.CharField(max_length=50)),
                ('usuario', models.OneToOneField(to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'db_table': 'perfiles',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Pregunta',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('descripcion', models.TextField()),
                ('urlImagen', models.URLField()),
                ('ultimoUso', models.IntegerField(default=0)),
                ('preguntasIncompatibles', models.ManyToManyField(related_name='preguntasIncompatibles_rel_+', db_table=b'preguntas_incompatibles', to='web.Pregunta')),
            ],
            options={
                'db_table': 'preguntas',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Respuesta',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('descripcion', models.TextField()),
                ('url_imagen', models.URLField()),
                ('correcta', models.BooleanField(default=False)),
                ('pregunta', models.ForeignKey(to='web.Pregunta')),
            ],
            options={
                'db_table': 'respuestas',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Tema',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nombre', models.CharField(max_length=100)),
                ('descripcion', models.TextField()),
                ('materia', models.ForeignKey(to='web.Materia')),
            ],
            options={
                'db_table': 'temas',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='TipoPregunta',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('tipo', models.CharField(max_length=100)),
                ('enunciado', models.CharField(max_length=200)),
            ],
            options={
                'db_table': 'tipos_pregunta',
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='pregunta',
            name='tema',
            field=models.ForeignKey(to='web.Tema'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='pregunta',
            name='tipo',
            field=models.ForeignKey(to='web.TipoPregunta'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='evaluacionpreguntarespuestausuario',
            name='pregunta',
            field=models.ForeignKey(to='web.Pregunta'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='evaluacionpreguntarespuestausuario',
            name='respuesta',
            field=models.ForeignKey(to='web.Respuesta', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='evaluacion',
            name='preguntas',
            field=models.ManyToManyField(to='web.Pregunta', db_table=b'evaluacion_pregunta'),
            preserve_default=True,
        ),
    ]
