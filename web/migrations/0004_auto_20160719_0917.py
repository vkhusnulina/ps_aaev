# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('web', '0003_evaluacion_materia'),
    ]

    operations = [
        migrations.AlterField(
            model_name='evaluacionesusuarios',
            name='clave_acceso',
            field=models.CharField(max_length=50, blank=True),
        ),
        migrations.AlterField(
            model_name='evaluacionesusuarios',
            name='fecha_hora_comienzo',
            field=models.DateTimeField(blank=True),
        ),
        migrations.AlterField(
            model_name='evaluacionesusuarios',
            name='fecha_hora_final',
            field=models.DateTimeField(blank=True),
        ),
        migrations.AlterField(
            model_name='evaluacionesusuarios',
            name='nota_final',
            field=models.FloatField(blank=True),
        ),
    ]
