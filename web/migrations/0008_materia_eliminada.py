# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('web', '0007_evaluacionpreguntarespuestausuario_evaluacion'),
    ]

    operations = [
        migrations.AddField(
            model_name='materia',
            name='eliminada',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
    ]
