from django.db import connection


class DBManager:
	_cursor = None

	def __init__(self):
		self._cursor = connection.cursor()

	@staticmethod
	def _dictfetchall(cursor):
		"""Returns all rows from a cursor as a dict"""
		desc = cursor.description
		return [
			dict(zip([col[0] for col in desc], row))
			for row in cursor.fetchall()
			]

	def query(self, query, params=[]):
		with self._cursor as c:
			c.execute(query, params)

	def select_all(self, query, params):
		self.query(self, query, params)
		return self._dictfetchall(self._cursor)

	# def initialSetUp(self):
