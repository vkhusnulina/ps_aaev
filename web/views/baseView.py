from functools import wraps

from django.shortcuts import redirect

# Para usar:
# @user_passes_test(lambda u: baseView.grant_permissions(u, ''))
from django.utils.decorators import available_attrs

from django.conf import settings

from web.models import Perfil


def grant_permission(u, permission):
    if u.has_perm('web.' + permission):
        return True
    else:
        redirect_to('home')


def _is_in_group(user, group_name):
    inGroup = False
    if user:
        inGroup = user.groups.filter(name=group_name)
    if inGroup:
        return True
    else:
        redirect_to('home')


# Por lo menos con permisos de Administrador
# Y solamente administrador - ya que es mayor jerarquia
def at_least_admin(user):
    return user.is_superuser


# Por lo menos profesor, pero tambien puede ser Administrador
def at_least_professor(user):
    if not user.is_staff:
        return at_least_admin(user)
    return True


# Por lo menos estudiante, al momento seria cada usuario logueado
def at_least_student(user):
    if user:
        return True
    return False


def redirect_to(url_name):
    return redirect(url_name)


def get_profile_obj(usuario):
    perfil = Perfil.objects.filter(usuario=usuario).first()
    if perfil is None:
        perfil = Perfil(
            usuario_id=usuario.id
        )
        perfil.save()
    return perfil


def security(test_func):
    def decorator(view_func):
        @wraps(view_func, assigned=available_attrs(view_func))
        def _wrapped_view(request, *args, **kwargs):
            request.session['is_auth'] = request.user.is_authenticated()
            if not request.session['is_auth']:
                return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))
            else:
                request.session['user_name'] = request.user.first_name + ' ' + request.user.last_name
                perfil = Perfil.objects.get(usuario=request.user)
                request.session['dni'] = perfil.dni
                if at_least_admin(request.user):
                    request.session['role'] = 'ADMIN'
                elif at_least_professor(request.user):
                    request.session['role'] = 'STAFF'
                else:
                    request.session['role'] = 'STUDENT'
            if test_func(request.user):
                return view_func(request, *args, **kwargs)
            return redirect('/')

        return _wrapped_view

    return decorator


def required_param(request, param):
    value = request.POST.get(param, None)
    if value and value.strip():
        return {'valid': True, 'value': value}
    else:
        return {'valid': False, 'value': 'El campo ' + param + ' es obligatorio.'}
