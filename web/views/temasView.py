"""
Created on 22/09/2014

@author: Valeria
"""
from django.template import RequestContext

# from forms import FormNuevaMateria
from django.shortcuts import render_to_response

from web.models import Tema, Materia, Pregunta, TipoPregunta
from django.views.decorators.csrf import csrf_exempt
from django.http.response import JsonResponse

from web.views import baseView
from web.views.baseView import security


@security(baseView.at_least_professor)
def listar_con_preguntas(request):
    materia_id = request.GET.get('materia_id')
    materia = Materia.objects.filter(pk=int(materia_id)).first()
    temas_obj = Tema.objects.filter(materia=materia)
    temas = []
    for tema in temas_obj:
        temas.append({'id': tema.id, 'nombre': tema.nombre})
    return JsonResponse(
        {'temas': temas, 'preguntas': _preguntas_por_materia(materia), 'materia_codigo': materia.codigo})


def _preguntas_por_materia(materia):
    preguntas = []
    preguntas_obj = Pregunta.objects.filter(tema__in=Tema.objects.filter(materia=materia))
    for pregunta in preguntas_obj:
        preguntas.append({'id': pregunta.id, 'enunciado': pregunta.descripcion, 'eliminada': pregunta.eliminada,
                          'tipo': pregunta.tipo.tipo, 'tema': pregunta.tema.nombre, 'tema_id': pregunta.tema.id})
    return preguntas


@security(baseView.at_least_professor)
@csrf_exempt
def crear(request):
    materia_id = request.POST.get('id')
    nombre = request.POST.get('nombre')
    descripcion = request.POST.get('descripcion')
    materia = Materia.objects.get(pk=int(materia_id))
    tema = Tema(materia=materia, nombre=nombre, descripcion=descripcion)
    tema.save()
    return JsonResponse({'id': tema.id, 'nombre': tema.nombre, 'descripcion': tema.descripcion})


@security(baseView.at_least_professor)
def get_index(request):
    materias = Materia.objects.all()
    # temas = Tema.objects.filter(materia_id=request.POST['valor'])
    return render_to_response('temas/agregar_tema.html', {'materias': materias},
                              context_instance=RequestContext(request))


@security(baseView.at_least_student)
def temas_de_materia(request, materia_id):
    temas = Tema.objects.filter(materia_id=int(materia_id))
    return render_to_response('temas/temas_de_materia.html', {'temas': temas, 'materia_id': materia_id},
                              context_instance=RequestContext(request))


@security(baseView.at_least_professor)
def temas_de_materia_combo(request, materia_id):
    temas = Tema.objects.filter(materia_id=int(materia_id))
    return render_to_response('temas/temas_de_materia_combo.html', {'temas': temas},
                              context_instance=RequestContext(request))
