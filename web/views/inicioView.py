# # views.py
from django.contrib.auth.models import User
from django.shortcuts import render_to_response, redirect
from django.template import RequestContext

from web.models import Perfil
from web.views import baseView
from web.views.baseView import at_least_admin, at_least_professor, security


def home(request):
    home_path = 'home.html'
    import os
    hostname = os.environ.get('DJANGO_DATABASE', 'NONE')
    request.session['user_name'] = ''
    request.session['dni'] = ''
    request.session['is_auth'] = False
    response = {
        'show_setup': False,
        'is_superuser': False,
        'hostname': hostname
    }
    try:
        total_users = User.objects.count()
        response['total_users'] = total_users
        if total_users > 0:
            request.session['is_auth'] = request.user.is_authenticated()
            if request.session['is_auth']:
                response['is_superuser'] = at_least_admin(request.user)
                request.session['user_name'] = request.user.first_name + ' ' + request.user.last_name
                perfil = Perfil.objects.get(usuario=request.user)
                request.session['dni'] = perfil.dni
                if at_least_admin(request.user):
                    request.session['role'] = 'ADMIN'
                elif at_least_professor(request.user):
                    request.session['role'] = 'STAFF'
                else:
                    request.session['role'] = 'STUDENT'
        else:
            response['show_setup'] = True
    except Exception:
        response['show_setup'] = True
    return render_to_response(home_path, response, context_instance=RequestContext(request))


def new_admin(request):
    if User.objects.count() > 0:
        return redirect('/')
    else:
        return render_to_response("users/nuevo_usuario_administrador.html", context_instance=RequestContext(request))


def ayuda(request):
    return render_to_response("ayuda.html", context_instance=RequestContext(request))


@security(baseView.at_least_student)
def perfil(request):
    perfil_usuario = baseView.get_profile_obj(request.user)
    return render_to_response('users/perfil.html', {
        'usuario': request.user,
        'perfil': perfil_usuario
    }, context_instance=RequestContext(request))
