# # views.py
from django.core.management import call_command
from django.http import JsonResponse
from django.shortcuts import redirect

from web.utils.dbManager import DBManager


def initial_setup(request=None):
	tiposPreguntaData = "INSERT IGNORE INTO tipos_pregunta (id, tipo, enunciado) " \
	                    "VALUES (1, 'Desarrollar', 'Responda las preguntas'), " \
	                    "(2, 'Seleccion multiple', 'Seleccione una o varias respuestas correctas'), " \
	                    "(3, 'Unir con flechas', 'Une con flechas los conceptos correspondientes'), " \
	                    "(4, 'Verdadero o falso', 'Indique si el concepto es verdadero o falso')"
	call_command('migrate', interactive=False)
	dbManager = DBManager()
	try:
		dbManager.query(tiposPreguntaData)
	except:
		pass
	return JsonResponse({})


def load_test_data(request=None):
	handleClear = open('remove_test_data.txt', 'r+')
	remove_test_data_query = handleClear.read()
	handleInsert = open('test_data.txt', 'r+')
	test_data_query = handleInsert.read()
	dbManager = DBManager()
	try:
		dbManager.query(remove_test_data_query + '; ' + test_data_query)
	except:
		pass
	return redirect('/', True)


def remove_test_data(request=None):
	handle = open('remove_test_data.txt', 'r+')
	test_data_query = handle.read()
	dbManager = DBManager()
	try:
		dbManager.query(test_data_query)
	except:
		pass
	return redirect('/', True)
