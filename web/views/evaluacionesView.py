import datetime

from web.utils import dbManager

try:
    from django.utils import simplejson as json
except:
    import json as json

from django.http import HttpResponse
from django.template import RequestContext
# To Response
from django.shortcuts import render_to_response, redirect

# Python library
import random
from random import shuffle
import time

# Model
from django.views.decorators.csrf import csrf_exempt

from web.models import Materia, Tema, Pregunta, TipoPregunta, Respuesta, Evaluacion, EvaluacionesUsuarios, \
    EvaluacionPreguntaRespuestaUsuario
from web.views import baseView
from web.views.baseView import security


@security(baseView.at_least_student)
def get_index(request):
    materias = Materia.get_list(request.user)
    evaluaciones = Evaluacion.get_list(request.user)
    return render_to_response('evaluaciones/index.html', {'materias': materias, 'evaluaciones': evaluaciones},
                              context_instance=RequestContext(request))


@security(baseView.at_least_professor)
def nueva_evaluacion_index(request):
    materias = Materia.objects.all()
    return render_to_response('evaluaciones/nueva_evaluacion_index.html', {'materias': materias},
                              context_instance=RequestContext(request))


@security(baseView.at_least_professor)
def asignar_evaluacion(request, evaluacion_id):
    evaluacion = Evaluacion.objects.get(pk=int(evaluacion_id))
    alumnos = evaluacion.materia.alumnos.all()
    for alumno in alumnos:
        if not EvaluacionesUsuarios.objects.filter(evaluacion=evaluacion, usuario=alumno).exists():
            EvaluacionesUsuarios.objects.create(
                evaluacion=evaluacion,
                usuario=alumno
            )
    return redirect('/evaluacion/ver/' + str(evaluacion.id))


@security(baseView.at_least_professor)
def nueva_evaluacion_configuraciones(request):
    materia = request.POST.get('materia')
    temas_str = request.POST.get('temas')
    tipos = TipoPregunta.objects.all()
    preguntas = []
    for tipo in tipos:
        preguntas.insert(tipo.id - 1, Pregunta.objects.filter(tema_id__in=temas_str.split(','), tipo_id=tipo.id))
        print preguntas[tipo.id - 1].count()
    return render_to_response('evaluaciones/configuraciones.html', {'temas': temas_str,
                                                                    'materia': materia,
                                                                    'tipos': tipos,
                                                                    'preguntas': preguntas},
                              context_instance=RequestContext(request))


@security(baseView.at_least_professor)
def temas_materia_checkbox(request, materia_id):
    temas = Tema.objects.filter(materia_id=int(materia_id))
    return render_to_response('temas/temas_materia_checkbox.html', {'temas': temas},
                              context_instance=RequestContext(request))


@security(baseView.at_least_professor)
def nueva_evaluacion(request):
    """	- Alta de Evaluacion misma """
    datatime = request.POST.get('inputDate')
    datatime = str(datatime[6:]) + "-" + str(datatime[3:5]) + "-" + str(datatime[:2]) + " " \
               + request.POST.get('inputHour') + ":" + request.POST.get('inputMinute') + ":00"
    date_now = time.strftime('%Y-%m-%d')

    materia = Materia.objects.get(id=int(request.POST.get('inputMateria')))
    evaluacion = (Evaluacion.objects.create(nombre=request.POST.get('inputName'),
                                            fecha_creacion=date_now,
                                            fecha_hora_comienzo=datatime,
                                            duracion_minutos=int(request.POST.get('inputMaxMinutes')),
                                            acepta_entrega_tardia=int(request.POST.get('inputAccept')),
                                            materia=materia
                                            ))

    """	- Seleccion aleatoria (con prioridades) de Preguntas para la evaluacion """
    """ - Cantidad de preguntas por tipo de pregunta sujeto a su disponibilidad """
    """ y presencia de preguntas incompatibles """
    """ - En consola se imprime el resumen de la aleatoriedad """
    temas_str = request.POST.get('temas')
    tipos = TipoPregunta.objects.all()
    preguntas_seleccionadas = []
    preguntas_incompatibles = []
    print "*************************************************"
    for tipo in tipos:
        preguntas = []
        print "-----------------------------------------------"
        print "Tipo " + tipo.tipo
        for x in range(0, int(request.POST.get('tipo' + str(tipo.id)))):
            preguntas_aleatorias = []
            preguntas = Pregunta.objects.filter(tema_id__in=temas_str.split(','), tipo_id=tipo.id).order_by(
                '-ultimoUso').exclude(id__in=preguntas_incompatibles)
            cantidad_preguntas = preguntas.count()
            if cantidad_preguntas > 0:
                mayor_aparicion = preguntas[0].ultimoUso
                print "Mayor aparicion " + str(mayor_aparicion)
                print "Cantidad de preguntas: " + str(cantidad_preguntas)
                for pregunta in preguntas:
                    cantidad_a_agregar = mayor_aparicion - pregunta.ultimoUso + 1
                    preguntas_aleatorias.extend([pregunta] * cantidad_a_agregar)
                max_random = len(preguntas_aleatorias) - 1
                pregunta_seleccionada = random.randint(0, max_random)
                print "Random " + str(preguntas_aleatorias[pregunta_seleccionada].id)
                print "Preguntas Aleatorias " + str(len(preguntas_aleatorias))
                preguntas_seleccionadas.append(preguntas_aleatorias[pregunta_seleccionada])
                preguntas_incompatibles.append(preguntas_aleatorias[pregunta_seleccionada].id)
                incompatibles = Pregunta.objects.filter(
                    preguntasIncompatibles__in=[preguntas_aleatorias[pregunta_seleccionada].id])
                for incompatible in incompatibles:
                    preguntas_incompatibles.append(incompatible.id)
                print "Preguntas Incompatibles"
                print preguntas_incompatibles
    print "Preguntas Seleccionadas " + str(len(preguntas_seleccionadas))
    print "*************************************************"
    for add_pregunta in preguntas_seleccionadas:
        evaluacion.preguntas.add(add_pregunta.id)
        print add_pregunta.id
        evaluacion.save()
        add_pregunta.ultimoUso += 1
        add_pregunta.save()
    return redirect('/evaluacion/ver/' + str(evaluacion.id))


@security(baseView.at_least_professor)
def mostrar_evaluacion(request, evaluacion_id):
    evaluacion = Evaluacion.objects.get(pk=int(evaluacion_id))
    tipos = TipoPregunta.objects.all()
    preguntas = []
    respuestas_join = []
    for tipo in tipos:
        preguntas.insert(tipo.id - 1, [])
    for pregunta in evaluacion.preguntas.all():
        preguntas[pregunta.tipo.id - 1].append([pregunta, Respuesta.objects.filter(pregunta_id=pregunta.id)])
        if pregunta.tipo.id == 3:
            respuestas_join.extend(Respuesta.objects.filter(pregunta_id=pregunta.id))
    shuffle(respuestas_join)
    return render_to_response('evaluaciones/preview.html',
                              {'evaluacion': evaluacion, 'tipos': tipos, 'preguntas': preguntas,
                               'respuestas_join': respuestas_join},
                              context_instance=RequestContext(request))


@security(baseView.at_least_student)
def mostrar_evaluacion_resolver(request, evaluacion_id):
    alumno_usuario_id = request.user
    evaluacion = Evaluacion.objects.get(pk=int(evaluacion_id))
    tipos = TipoPregunta.objects.all()
    preguntas = []
    respuestas_join = []
    for tipo in tipos:
        preguntas.insert(tipo.id - 1, [])
    print preguntas
    for pregunta in evaluacion.preguntas.all():
        respuesta = Respuesta.objects.filter(pregunta_id=pregunta.id)
        respuesta_del_alumno = EvaluacionPreguntaRespuestaUsuario.objects.filter(pregunta_id=pregunta.id,
                                                                                 alumno_id=alumno_usuario_id,
                                                                                 evaluacion_id=evaluacion)
        preguntas[pregunta.tipo.id - 1].append([pregunta, respuesta, respuesta_del_alumno])
        print preguntas[pregunta.tipo.id - 1]
        if pregunta.tipo.id == TipoPregunta.unir_con_flecha():
            respuestas_join.extend(respuesta)
    shuffle(respuestas_join)
    evaluacion_del_alumno = EvaluacionesUsuarios.objects.get(evaluacion=evaluacion, usuario=alumno_usuario_id)

    timeremind = _calcular_tiempo_de_evaluacion(evaluacion, evaluacion_del_alumno)

    return render_to_response('evaluaciones/ver.html', {
        'evaluacion': evaluacion,
        'tipos': tipos,
        'preguntas': preguntas,
        'respuestas_join': respuestas_join,
        'timeremind': timeremind
    }, context_instance=RequestContext(request))


def _calcular_tiempo_de_evaluacion(evaluacion, evaluacion_del_alumno):
    if not evaluacion_del_alumno.fecha_hora_comienzo:
        evaluacion_del_alumno.fecha_hora_comienzo = time.strftime('%Y-%m-%d %H:%M:%S')
        evaluacion_del_alumno.save()
        timeremind = evaluacion.duracion_minutos
    else:
        t1, t2 = evaluacion_del_alumno.fecha_hora_comienzo.time(), datetime.datetime.now().time()
        dt1 = datetime.timedelta(hours=t1.hour, minutes=t1.minute, seconds=t1.second, microseconds=t1.microsecond)
        dt2 = datetime.timedelta(hours=t2.hour, minutes=t2.minute, seconds=t2.second, microseconds=t2.microsecond)
        diff = dt2 - dt1
        timeremind = int(evaluacion.duracion_minutos) - (diff.seconds / 60)
        if timeremind < 0:
            timeremind = 0
    return timeremind


@security(baseView.at_least_student)
@csrf_exempt
def evaluacion_resolver(request, evaluacion_id):
    received_json_data = json.loads(request.body)
    preguntas = received_json_data['preguntas']
    evaluacion = Evaluacion.objects.get(id=evaluacion_id)
    total_preguntas = evaluacion.preguntas.count()
    alumno = request.user
    EvaluacionPreguntaRespuestaUsuario.objects.filter(alumno=alumno, evaluacion=evaluacion).all().delete()
    total_correctas = 0
    total = total_preguntas
    for preguntaJsonIterator in preguntas:
        preguntaJson = preguntaJsonIterator.itervalues().next()
        pregunta = Pregunta.objects.get(id=preguntaJson['id'])
        if preguntaJson['tipo'] == 1:
            EvaluacionPreguntaRespuestaUsuario.objects.create(alumno=alumno,
                                                              evaluacion=evaluacion,
                                                              pregunta=pregunta,
                                                              respuesta_texto=preguntaJson['texto']
                                                              )
        for respuestaId in preguntaJson['respuestas']:
            respuesta = Respuesta.objects.get(id=respuestaId)
            if respuesta.correcta:
                total_correctas += 1
            EvaluacionPreguntaRespuestaUsuario.objects.create(alumno=alumno,
                                                              evaluacion=evaluacion,
                                                              pregunta=pregunta,
                                                              respuesta=respuesta
                                                              )
    nota = 100 / total * total_correctas
    evaluacion_alumno = EvaluacionesUsuarios.objects.get(usuario=alumno, evaluacion=evaluacion)
    evaluacion_alumno.nota_final = nota
    evaluacion_alumno.fecha_hora_final = time.strftime('%Y-%m-%d %H:%M:%S')
    evaluacion_alumno.save()
    return HttpResponse(json.dumps({'nota': nota}), content_type="application/json")


@security(baseView.at_least_professor)
def evaluar(request, evaluacion_id, alumno_id):
    evaluacion_alumno = EvaluacionesUsuarios.objects.get(usuario=alumno_id, evaluacion=evaluacion_id)
    evaluacion = evaluacion_alumno.evaluacion
    tipos = TipoPregunta.objects.all()
    preguntas_colection = []
    for tipo in tipos:
        preguntas_colection.insert(tipo.id - 1, [])
    preguntas = evaluacion.preguntas.all()
    for pregunta in preguntas:
        pregunta.respuestas = Respuesta.objects.filter(pregunta_id=pregunta.id)
        if pregunta.tipo.id == TipoPregunta.seleccion_multiple():
            for respuesta in pregunta.respuestas:
                respuesta.alumno_respuesta = EvaluacionPreguntaRespuestaUsuario.objects.filter(pregunta_id=pregunta.id,
                                                                                               alumno_id=alumno_id,
                                                                                               evaluacion_id=evaluacion,
                                                                                               respuesta_id=respuesta.id)
            pregunta.respuesta_alumno = []
        else:
            pregunta.respuesta_alumno = EvaluacionPreguntaRespuestaUsuario.objects.filter(pregunta_id=pregunta.id,
                                                                                          alumno_id=alumno_id,
                                                                                          evaluacion_id=evaluacion)
        preguntas_colection[pregunta.tipo.id - 1].append(pregunta)

    return render_to_response('evaluaciones/evaluar.html', {
        'evaluacion_alumno': evaluacion_alumno,
        'evaluacion': evaluacion,
        'tipos': tipos,
        'preguntas': preguntas_colection
    }, context_instance=RequestContext(request))
