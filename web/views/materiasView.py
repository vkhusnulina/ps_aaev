"""
Created on 22/09/2014

@author: Valeria
"""
from django.contrib.auth.models import User
from django.http import JsonResponse
from django.shortcuts import render_to_response, redirect
from django.template import RequestContext
from django.views.decorators.csrf import csrf_exempt

from web.models import Materia, Tema, Evaluacion
from web.views import baseView
from web.views.baseView import security, at_least_admin


@security(baseView.at_least_professor)
@csrf_exempt
def nueva_materia(request):
    descripcion = request.POST.get('descripcion', '');
    errors = []
    values = {}

    value = baseView.required_param(request, 'codigo')
    if not value['valid']:
        errors.append(value['value'])
    else:
        values['codigo'] = value['value']

    value = baseView.required_param(request, 'nombre')
    if not value['valid']:
        errors.append(value['value'])
    else:
        values['nombre'] = value['value']

    if len(errors) == 0:
        duplicados = Materia.objects.filter(codigo=values['codigo']).count()
        if duplicados > 0:
            errors.append("La materia con el codigo " + values['codigo'] + " ya existe en el sistema.")
        else:
            materia = Materia(
                codigo=values['codigo'],
                nombre=values['nombre'],
                descripcion=descripcion,
                creador_id=request.user.id)
            materia.save()
            if not request.user.is_superuser:
                docente = User.objects.get(id=request.user.id)
                materia.docentes.add(docente)
    if len(errors) == 0:
        return JsonResponse({'msg': values['codigo'], 'valid': True})
    else:
        return JsonResponse({'msg': errors, 'valid': False})


@security(baseView.at_least_student)
def listar(request):
    if request.method == 'POST':
        if request.POST.get('inputAction') == 'mod':
            modmateria = Materia.objects.get(id=request.POST.get('inputId'))
            modmateria.codigo = request.POST.get('inputCodigo')
            modmateria.nombre = request.POST.get('inputNombre')
            modmateria.descripcion = request.POST.get('inputDescripcion')
            modmateria.save()
        if request.POST.get('inputAction') == 'assign':
            materia = Materia.objects.get(id=request.POST.get('inputId'))
            docente = User.objects.get(id=request.POST.get('inputDocente'))
            if Materia.objects.filter(docentes__id=docente.id, id=materia.id).exists():
                materia.docentes.remove(docente)
            else:
                materia.docentes.add(docente)
    materias = Materia.get_list(request.user)
    docentes = {}
    if at_least_admin(request.user):
        docentes = User.objects.filter(is_superuser=0, is_staff=1).all()
    return render_to_response('materias/lista_materia.html', {'materias': materias, 'docentes': docentes},
                              context_instance=RequestContext(request))


@security(baseView.at_least_admin)
@csrf_exempt
def inhabilitar(request):
    materia_id = request.POST.get('id')
    materia = Materia.objects.filter(pk=int(materia_id)).first()
    materia.eliminada = True
    materia.save()
    return JsonResponse({})


@security(baseView.at_least_admin)
@csrf_exempt
def habilitar(request):
    materia_id = request.POST.get('id')
    materia = Materia.objects.filter(pk=int(materia_id)).first()
    materia.eliminada = False
    materia.save()
    return JsonResponse({})


@security(baseView.at_least_professor)
@csrf_exempt
def editar(request):
    entity_id = request.POST.get('id')
    campo = request.POST.get('field')
    valor = request.POST.get('value')
    entity = request.POST.get('entity')
    saved = True
    if entity == 'MATERIA':
        entity_obj = Materia.objects.filter(pk=int(entity_id)).first()
    else:
        entity_obj = Tema.objects.filter(pk=int(entity_id)).first()
    if campo == 'nombre':
        entity_obj.nombre = valor
    elif campo == 'descripcion':
        entity_obj.descripcion = valor
    else:
        saved = False
    if saved:
        entity_obj.save()
    return JsonResponse({'saved': saved})


@security(baseView.at_least_admin)
@csrf_exempt
def docentes(request):
    materia_id = request.GET.get('id')
    materia = Materia.objects.filter(pk=materia_id).first()
    obj_asignados = materia.docentes.all()
    asignados = []
    for asignado in obj_asignados:
        asignados.append({
            'first_name': asignado.first_name,
            'last_name': asignado.last_name,
            'id': asignado.id
        })
    obj_docentes = User.objects.filter(is_superuser=False, is_staff=True).exclude(id__in=obj_asignados)
    docentes_list = []
    for docente in obj_docentes:
        docentes_list.append({
            'first_name': docente.first_name,
            'last_name': docente.last_name,
            'id': docente.id
        })
    return JsonResponse({'asignados': asignados, 'otros': docentes_list})


@security(baseView.at_least_admin)
@csrf_exempt
def docente(request):
    materia_id = request.POST.get('materia_id')
    docente_id = request.POST.get('docente_id')
    accion = request.POST.get('accion')
    updated = False

    materia = Materia.objects.filter(pk=materia_id).first()
    docente_obj = User.objects.filter(pk=docente_id).first()
    if accion == 'ADD':
        materia.docentes.add(docente_obj)
        updated = True
    elif accion == 'REMOVE':
        materia.docentes.remove(docente_obj)
        updated = True
    return JsonResponse({'updated': updated})


@security(baseView.at_least_student)
def perfil(request, code):
    materia = Materia.objects.filter(codigo=int(code)).first()
    temas = Tema.objects.filter(materia=materia).all()
    alumnos = materia.alumnos.all()
    evaluaciones = Evaluacion.objects.filter(materia=materia)
    puede_editar = _puede_editar_materia(request.user, materia)
    return render_to_response('materias/perfil_materia.html',
                              {
                                  'materia': materia,
                                  'temas': temas,
                                  'alumnos': alumnos,
                                  'evaluaciones': evaluaciones,
                                  'puede_editar': puede_editar
                              },
                              context_instance=RequestContext(request))


def _puede_editar_materia(user, materia):
    if user.is_superuser:
        return True
    elif user.is_staff:
        """docente = Materia.objects.filter(id=materia.id, docentes__docente=user.id).first()
        if docente:
            return True"""
        return True
    return False
