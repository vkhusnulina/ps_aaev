"""
Created on 22/09/2014

@author: Valeria
"""
from django.template import RequestContext

# from forms import FormNuevaMateria
from django.shortcuts import render_to_response
from django.http.response import JsonResponse
from django.views.decorators.csrf import csrf_exempt

from web.views import baseView
from web.views.baseView import security

from web.models import Materia, Tema, Pregunta, TipoPregunta, Respuesta


@security(baseView.at_least_professor)
def preguntas_incompatibles(request, tema_id):
    preguntas = []
    preguntas.extend(Pregunta.objects.filter(tema_id=tema_id))
    return render_to_response('preguntas/tabla_preguntas.html', {'preguntas': preguntas},
                              context_instance=RequestContext(request))


@security(baseView.at_least_professor)
@csrf_exempt
def nueva_pregunta(request):
    incompatibles = request.POST.get('preguntas_incompatibles')
    incompatibles = incompatibles.split('&')
    nuevaPregunta = (Pregunta.objects.create(tipo_id=request.POST.get('tipo_id'),
                                             tema_id=request.POST.get('tema_id'),
                                             descripcion=request.POST.get('enunciado'),
                                             urlImagen=request.POST.get('enunciado_grafico', ''),
                                             ultimoUso=0
                                             ))
    if request.POST.get('preguntas_incompatibles') != '':
        for inc_id in incompatibles:
            nuevaPregunta.preguntasIncompatibles.add(inc_id)
            nuevaPregunta.save()
    cantidad_respuestas = request.POST.get('cantidad_respuestas')
    for x in range(1, int(cantidad_respuestas) + 1):
        respuesta = (Respuesta.objects.create(pregunta_id=nuevaPregunta.id,
                                              descripcion=request.POST.get('respuesta' + str(x)),
                                              url_imagen="",
                                              correcta=int(request.POST.get('valor' + str(x)))
                                              ))
    response_data = {'errors': "sin errores"}
    return JsonResponse(response_data)


@security(baseView.at_least_professor)
def listar(request):
    materias = Materia.get_list(request.user)
    tipos_pregunta = TipoPregunta.objects.all()
    return render_to_response('preguntas/index.html', {'materias': materias, 'materia_seleccionar': materias[0].id,
                                                       'tipos_pregunta': tipos_pregunta},
                              context_instance=RequestContext(request))


@security(baseView.at_least_professor)
def ver(request, pregunta_id):
    pregunta = Pregunta.objects.filter(id=pregunta_id).first()
    respuestas_obj = Respuesta.objects.filter(pregunta=pregunta)
    respuestas = []
    for respuesta in respuestas_obj:
        respuestas.append({'id': respuesta.id, 'pregunta': respuesta.descripcion,
                           'correcta': respuesta.correcta})
    response = {'id': pregunta.id, 'enunciado': pregunta.descripcion, 'imagen': pregunta.urlImagen,
                'tipo': pregunta.tipo.tipo, 'tipo_id': 'tipo_' + str(pregunta.tipo.id), 'tema': pregunta.tema.nombre,
                'respuestas': respuestas}
    return JsonResponse(response)


@security(baseView.at_least_professor)
@csrf_exempt
def editar(request):
    pregunta_id = request.POST.get('id')
    validation = _validar_pregunta(request)
    if len(validation['errors']) > 0:
        return JsonResponse({'valid': False, 'errors': validation['errors']})
    else:
        values = validation['values']
        pregunta = Pregunta.objects.filter(pk=int(pregunta_id)).first()
        pregunta.descripcion = values['enunciado']
        pregunta.urlImagen = request.POST.get('enunciado_grafico', '')
        pregunta.ultimoUso = 0
        pregunta.save()

        respuestas = request.POST
        respuestas_obj = Respuesta.objects.filter(pregunta=pregunta)
        for respuesta in respuestas_obj:
            if respuestas.get('respuestas[' + str(respuesta.id) + '][respuesta]', False):
                respuesta.correcta = True if (
                    respuestas.get('respuestas[' + str(respuesta.id) + '][correcta]', 'false') == 'true') else False
                respuesta.descripcion = respuestas.get('respuestas[' + str(respuesta.id) + '][respuesta]', '')
                respuesta.save()
        return JsonResponse({'valid': True, 'errors': []})


def _validar_pregunta(request):
    errors = []
    values = {}

    value = baseView.required_param(request, 'enunciado')
    if not value['valid']:
        errors.append(value['value'])
    else:
        values['enunciado'] = value['value']

    return {'values': values, 'errors': errors}


@security(baseView.at_least_professor)
@csrf_exempt
def inhabilitar(request):
    pregunta_id = request.POST.get('id')
    pregunta = Pregunta.objects.filter(pk=int(pregunta_id)).first()
    pregunta.eliminada = True
    pregunta.save()
    return JsonResponse({})


@security(baseView.at_least_professor)
@csrf_exempt
def habilitar(request):
    pregunta_id = request.POST.get('id')
    pregunta = Pregunta.objects.filter(pk=int(pregunta_id)).first()
    pregunta.eliminada = False
    pregunta.save()
    return JsonResponse({})


@security(baseView.at_least_professor)
def respuestas_form(request, pregunta_id):
    respuestas = Respuesta.objects.filter(pregunta_id=pregunta_id)
    pregunta = Pregunta.objects.get(pk=int(pregunta_id))
    if pregunta.tipo.id == 4:
        editar = False
    else:
        editar = True
    return render_to_response('preguntas/respuestas_form.html', {'respuestas': respuestas, 'editar': editar},
                              context_instance=RequestContext(request))


@security(baseView.at_least_professor)
def lista_pregunta(request):
    if request.method == 'POST':
        pregunta = Pregunta.objects.get(id=request.POST.get('inputId'))
        tema_id = pregunta.tema.id
        materias = Materia.get_list(request.user)
        tipos_pregunta = TipoPregunta.objects.all()
        tema = Tema.objects.get(pk=int(tema_id))
        return render_to_response('preguntas/index.html', {'materias': materias, 'materia_seleccionar': tema.materia.id,
                                                           'tipos_pregunta': tipos_pregunta},
                                  context_instance=RequestContext(request))
