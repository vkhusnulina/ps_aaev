"""
Created on 22/09/2014

@author: Valeria
"""
import csv

from django.contrib.auth import login, authenticate
from django.contrib.auth.models import User
from django.http.response import JsonResponse
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.views.decorators.csrf import csrf_exempt

from web.views import baseView
from web.views.baseView import security

try:
    from django.utils import simplejson as json
except:
    import json as json
from web.models import Materia, Perfil, Evaluacion, EvaluacionesUsuarios


# ------- Usuarios
@security(baseView.at_least_student)
@csrf_exempt
def editar_usuario(request):
    user_id = request.POST.get('id')
    campo = request.POST.get('field')
    valor = request.POST.get('value')
    saved = True
    msg = ""

    if valor == '' or campo == '':
        saved = False
        msg = "Los datos de usuarios son requeridos."
    else:
        usuario = User.objects.filter(pk=int(user_id)).first()

        if campo == 'nombre':
            usuario.first_name = valor
        elif campo == 'apellido':
            usuario.last_name = valor
        elif campo == 'email':
            duplicados = User.objects.filter(email=valor).exclude(pk=int(user_id)).count()
            if duplicados == 0:
                usuario.email = valor
            else:
                saved = False
                msg = "El usuario con mismo email ya existe en el sistema."
        elif campo == 'username':
            duplicados = User.objects.filter(username=valor).exclude(pk=int(user_id)).count()
            if duplicados == 0:
                usuario.username = valor
            else:
                saved = False
                msg = "El usuario con mismo username ya existe en el sistema."
        elif campo == 'password':
            if 6 > len(valor) > 10:
                saved = False
                msg = "La password debe contener entre 6 a 10 caracteres."
            else:
                usuario.set_password(valor)
        elif campo == 'dni':
            perfil = Perfil.objects.filter(usuario=usuario).first()
            perfil.dni = valor
            perfil.save()
        else:
            saved = False
            msg = "Campo a modificar incorrecto."
    if saved:
        usuario.save()
    return JsonResponse({'saved': saved, 'msg': msg})


# ------- Administradores

@csrf_exempt
def new_admin_user(request):  # New Admin User
    errors = {}
    email = request.POST.get('inputEmail')
    username = request.POST.get('inputUser')
    firstname = request.POST.get('inputName')
    lastname = request.POST.get('inputLast')
    password = request.POST.get('inputPassword')
    user = User.objects.create_superuser(username=username,
                                         email=email,
                                         password=password,
                                         first_name=firstname,
                                         last_name=lastname)
    Perfil.objects.create(usuario=user, dni="[ADMINISTRADOR]")
    user_auth = authenticate(username=username, password=password)
    login(request, user_auth)
    return JsonResponse(errors)


# ------- Profesores

@csrf_exempt
@security(baseView.at_least_admin)
def agregar_profesor(request):
    if request.method == 'POST':
        validation = _validar_nuevo_profesor(request)
        if len(validation['errors']) > 0:
            return JsonResponse({'valid': False, 'errors': validation['errors']})
        else:
            values = validation['values']
            user = User.objects.create_user(username=values['username'],
                                            email=values['email'],
                                            password=values['password'],
                                            first_name=values['nombre'],
                                            last_name=values['apellido'])
            user.is_staff = True
            user.save()
            Perfil.objects.create(usuario=user, dni=values['dni'])
            return JsonResponse({'valid': True, 'errors': []})
    else:  # Case of GET request
        return render_to_response("users/agregar_profesor.html", context_instance=RequestContext(request))


def _validar_nuevo_profesor(request):
    errors = []
    values = {}

    value = baseView.required_param(request, 'email')
    if not value['valid']:
        errors.append(value['value'])
    else:
        values['email'] = value['value']

    value = baseView.required_param(request, 'username')
    if not value['valid']:
        errors.append(value['value'])
    else:
        values['username'] = value['value']

    value = baseView.required_param(request, 'nombre')
    if not value['valid']:
        errors.append(value['value'])
    else:
        values['nombre'] = value['value']

    value = baseView.required_param(request, 'apellido')
    if not value['valid']:
        errors.append(value['value'])
    else:
        values['apellido'] = value['value']

    value = baseView.required_param(request, 'password')
    if not value['valid']:
        errors.append(value['value'])
    else:
        values['password'] = value['value']

    value = baseView.required_param(request, 'dni')
    if not value['valid']:
        errors.append(value['value'])
    else:
        values['dni'] = value['value']

    if len(errors) == 0:
        duplicados = User.objects.filter(email=values['email']).count()
        if duplicados > 0:
            errors.append("El usuario con mismo email ya existe en el sistema.")

        duplicados = User.objects.filter(username=values['username']).count()
        if duplicados > 0:
            errors.append("El usuario con mismo username ya existe en el sistema.")

        if 6 > len(values['password']) > 10:
            errors.append("La password debe contener entre 6 a 10 caracteres.")

    return {'values': values, 'errors': errors}


@security(baseView.at_least_admin)
def profesores(request):
    usuarios = User.objects.filter(is_superuser=False, is_staff=True)
    return render_to_response('users/profesores.html', {'usuarios': usuarios}, context_instance=RequestContext(request))


@security(baseView.at_least_admin)
def ver_profesor(request, profesor_id):
    profesor = User.objects.get(id=profesor_id)
    perfil = baseView.get_profile_obj(profesor)
    materias = Materia.objects.filter(docentes__id=profesor.id)
    return render_to_response('users/perfil_profesor.html', {
        'profesor': profesor,
        'perfil': perfil,
        'materias': materias
    }, context_instance=RequestContext(request))


# ------- Alumnos

@security(baseView.at_least_professor)
@csrf_exempt
def leer_csv(request):
    rows = {}
    errors = []
    if request.FILES:
        request_file = request.FILES.get('file_upload')
        reader = csv.reader(request_file, delimiter=';', quoting=csv.QUOTE_NONE)
        i = 0
        for row in reader:
            try:
                rows[i] = {}
                rows[i]["dni"] = row[2]
                rows[i]["firstName"] = row[0]
                rows[i]["lastName"] = row[1]
                rows[i]["email"] = row[3]
                i += 1
            except Exception:
                errors.append('Error en la linea "' + ';'.join(row) + '"')
                pass
    response_data = {'users': rows, 'errors': errors}
    return JsonResponse(response_data)


@csrf_exempt
@security(baseView.at_least_professor)
def agregar_alumnos(request):
    materias = Materia.get_list(request.user)
    return render_to_response("users/agregar_alumnos.html", {'materias': materias},
                              context_instance=RequestContext(request))


@security(baseView.at_least_professor)
@csrf_exempt
def procesar_alumnos(request):
    errors = []
    asignados = 0
    actualizados = 0
    agregados = 0
    asignar = False
    solo_asignar = (request.POST.get('soloAsignar', 'off') == 'on')
    asignar_a_materia = request.POST.get('asignarAlumnosAMateria', 'off')
    if asignar_a_materia == 'on':
        asignar_a_materia = Materia.objects.filter(id=request.POST.get('idMateria')).first()
        if asignar_a_materia:
            asignar = True
    reemplazar = (request.POST.get('reemplazarDatosDelAlumno', 'off') == 'on')

    i = request.POST.get('cantidad')
    i = i.split("_")

    for x in range(1, len(i)):
        if User.objects.filter(email=request.POST.get('mail' + i[x])).exists() or Perfil.objects.filter(
                dni=request.POST.get('dni' + i[x])).exists():
            user = User.objects.filter(email=request.POST.get('mail' + i[x])).first()
            if not user:
                perfil = Perfil.objects.filter(
                    dni=request.POST.get('dni' + i[x])).first()
                user = perfil.usuario

            if reemplazar and not solo_asignar:
                user.first_name = request.POST.get('name' + i[x])
                user.last_name = request.POST.get('last' + i[x])
                user.save()
                actualizados += 1

            if asignar:
                asignar_a_materia.alumnos.add(user)
                asignados += 1

            if not reemplazar and not asignar:
                errors.append("El usuario con el dni " + request.POST.get('dni' + i[x]) + " ya se encuentra registrado")
        elif not solo_asignar:
            user = User.objects.create_user(username="DNI" + request.POST.get('dni' + i[x]),
                                            email=request.POST.get('mail' + i[x]),
                                            password=request.POST.get('dni' + i[x]),
                                            first_name=request.POST.get('name' + i[x]),
                                            last_name=request.POST.get('last' + i[x]))
            Perfil.objects.create(usuario=user, dni=request.POST.get('dni' + i[x]))
            agregados += 1
            if asignar:
                asignar_a_materia.alumnos.add(user)
                asignados += 1
        else:
            errors.append("El usuario con el dni " + request.POST.get('dni' + i[x]) + " y/o email " + request.POST.get(
                'mail' + i[x]) + " no se encuentra registrado")

    response_data = {'errors': errors, 'agregados': agregados, 'actualizados': actualizados, 'asignados': asignados}
    return JsonResponse(response_data)


@security(baseView.at_least_professor)
def alumnos(request):
    if request.method == 'POST':
        if request.POST.get('inputAction') == 'baja':
            delUsuario = User.objects.get(id=request.POST.get('inputId'))
            delUsuario.delete()
        if request.POST.get('inputAction') == 'mod':
            modUsuario = User.objects.get(id=request.POST.get('inputId'))
            modUsuario.first_name = request.POST.get('inputNombre')
            modUsuario.last_name = request.POST.get('inputApellido')
            modUsuario.username = request.POST.get('inputDni')
            modUsuario.email = request.POST.get('inputMail')
            modUsuario.save()

    usuarios = User.objects.filter(is_superuser=False, is_staff=False)
    return render_to_response('users/alumnos.html', {'usuarios': usuarios}, context_instance=RequestContext(request))


@security(baseView.at_least_professor)
@csrf_exempt
def ver_alumno(request, alumno_id):
    alumno = User.objects.get(id=alumno_id)
    perfil = baseView.get_profile_obj(alumno)
    materias = Materia.objects.filter(alumnos=alumno_id)
    for materia in materias:
        evaluaciones = Evaluacion.objects.filter(materia_id=materia.id)
        for evaluacion in evaluaciones:
            asignacion = EvaluacionesUsuarios.objects.filter(evaluacion_id=evaluacion.id, usuario=alumno_id).first()
            evaluacion.asignacion = asignacion
        materia.evaluaciones = evaluaciones
    return render_to_response('users/perfil_alumno.html', {
        'alumno': alumno,
        'perfil': perfil,
        'materias': materias
    }, context_instance=RequestContext(request))
